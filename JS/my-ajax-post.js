
"use strict";

var ajaxRequest = $.ajax("data/my-ajax-blog.json");

ajaxRequest.done(function (data) {

    console.log(data);

    var dataToHTML = buildHTML(data);

    $("#my-posts").html(dataToHTML);

});

function buildHTML(posts) {

    var postHTML = "";

    posts.forEach(function (post) {

    postHTML += `
    
    <h3>${post.title}</h3>
    
    <br>
    
    <img src="${post.pic}" height="675" width="1012"/>
    
    <br>
    
    <h6>${post.content}</h6>
    
    <br>
    
    <p><strong>Categories: </strong>${post.categories}</p>
    
    <br>
    
    <p><strong>Projected Date: </strong>${post.date}</p>
    
    <br>
    <br>
        
        `;

    });

    return postHTML;

};